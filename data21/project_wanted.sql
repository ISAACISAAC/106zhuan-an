-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: project
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wanted`
--

DROP TABLE IF EXISTS `wanted`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wanted` (
  `idwanted` int(11) NOT NULL AUTO_INCREMENT,
  `context` varchar(45) NOT NULL,
  `amount` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `time` varchar(45) NOT NULL DEFAULT '1',
  `author` int(11) NOT NULL,
  `credit` double NOT NULL,
  PRIMARY KEY (`idwanted`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wanted`
--

LOCK TABLES `wanted` WRITE;
/*!40000 ALTER TABLE `wanted` DISABLE KEYS */;
INSERT INTO `wanted` VALUES (1,'如題 300金%#%',300,'求台大事件懶人包','2018-05-08 13:55',17,99),(26,'請大家幫忙%#%',500,'球員交易內幕','2018-05-11 16:58',5,18.666666666666668),(29,'有人有卦嗎?%#%',3000,'政治獻金內幕','2018-06-01 11:44',1495,46.25),(30,'希望大家能幫我找出兇手!<br>還我弟一個公道!%#%',1000,'求爆炸影片','2018-06-05 14:12',10,55),(31,'希望大家能幫我找出兇手!<br><br>還我弟一個公道!<br><br>10000元%#%',60,'有無神之卡詳情','2018-06-05 20:45',1,55),(32,'希望大家能幫我找出兇手!<br><br>還我弟一個公道!<br><br>10000元%#%',10000,'求政大商院霸凌事件','2018-06-05 20:48',2,55);
/*!40000 ALTER TABLE `wanted` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-08 16:59:54
