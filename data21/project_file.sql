-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: project
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
  `idfile` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(45) DEFAULT NULL,
  `filepath` varchar(500) NOT NULL,
  `articleid` int(11) DEFAULT NULL,
  `newsid` int(11) DEFAULT NULL,
  `wantedid` int(11) DEFAULT NULL,
  `commentid` int(11) DEFAULT NULL,
  PRIMARY KEY (`idfile`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file`
--

LOCK TABLES `file` WRITE;
/*!40000 ALTER TABLE `file` DISABLE KEYS */;
INSERT INTO `file` VALUES (1,'File1','css\\\\img\\\\7.png',27,NULL,NULL,NULL),(2,'File1','css\\\\img\\\\7.png',32,NULL,NULL,NULL),(3,'File1','css\\\\img\\\\7.png',33,NULL,NULL,NULL),(4,'File1','css\\\\img\\\\7.png',34,NULL,NULL,NULL),(5,'File1','css\\\\img\\\\7.png',35,NULL,NULL,NULL),(6,'File1','css\\\\img\\\\7.png',36,NULL,NULL,NULL),(7,'File1','css\\\\img\\\\7.png',38,NULL,NULL,NULL),(8,'File1','css\\\\img\\\\8.png',39,NULL,NULL,NULL),(9,'File1','css\\\\img\\\\9.png',40,NULL,NULL,NULL),(10,'File1','css\\\\img\\\\10.png',41,NULL,NULL,NULL),(11,NULL,'css\\\\img\\\\11.jpg',47,NULL,NULL,NULL),(12,NULL,'css\\\\img\\\\12.png',54,NULL,NULL,NULL),(13,NULL,'css\\\\img\\\\13.png',55,NULL,NULL,NULL),(14,NULL,'css\\\\img\\\\14.png',56,NULL,NULL,NULL),(15,NULL,'css\\\\img\\\\15.jpg',57,NULL,NULL,NULL),(16,NULL,'css\\\\img\\\\16.jpg',58,NULL,NULL,NULL),(17,NULL,'css\\\\img\\\\17.mp4',59,NULL,NULL,NULL),(18,NULL,'css\\\\img\\\\18.mp4',60,NULL,NULL,NULL),(19,NULL,'css\\\\img\\\\19.png',61,NULL,NULL,NULL),(20,NULL,'css\\\\img\\\\20.jpg',62,NULL,NULL,NULL),(21,NULL,'css\\\\img\\\\21.png',63,NULL,NULL,NULL),(22,NULL,'css\\\\img\\\\22.png',64,NULL,NULL,NULL),(23,NULL,'css\\\\img\\\\23.png',65,NULL,NULL,NULL),(24,NULL,'css\\\\img\\\\24.png',66,NULL,NULL,NULL),(25,NULL,'css\\\\img\\\\25.mp4',71,NULL,NULL,NULL),(26,NULL,'wanted\\\\26.doc',NULL,NULL,4013,NULL),(27,NULL,'wanted\\\\27.jpg',NULL,NULL,4028,NULL),(28,NULL,'wanted\\\\28.png',NULL,NULL,4034,NULL),(29,NULL,'css\\\\img\\\\29.png',NULL,NULL,3,NULL),(30,NULL,'css\\\\img\\\\30.png',112,NULL,NULL,NULL),(31,NULL,'css\\\\img\\\\31.png',115,NULL,NULL,NULL),(32,NULL,'css\\\\img\\\\32.png',116,NULL,NULL,NULL),(33,NULL,'css\\\\img\\\\33.png',117,NULL,NULL,NULL),(34,NULL,'wanted\\\\34.png',NULL,NULL,4038,NULL),(37,NULL,'css\\\\img\\\\37.png',119,NULL,NULL,NULL),(38,NULL,'css\\\\img\\\\38.png',NULL,NULL,24,NULL),(39,NULL,'css\\\\img\\\\39.png',122,NULL,NULL,NULL),(40,NULL,'css\\\\img\\\\40.png',123,NULL,NULL,NULL),(41,NULL,'css\\\\img\\\\41.png',123,NULL,NULL,NULL),(42,NULL,'css\\\\img\\\\42.png',125,NULL,NULL,NULL),(43,NULL,'css\\\\img\\\\43.png',123,NULL,NULL,NULL),(44,NULL,'css\\\\img\\\\44.png',127,NULL,NULL,NULL),(45,NULL,'css\\\\img\\\\45.png',123,NULL,NULL,NULL),(46,NULL,'css\\\\img\\\\46.png',134,NULL,NULL,NULL),(47,NULL,'wanted\\\\47.png',NULL,NULL,4074,NULL),(48,NULL,'wanted\\\\48.png',NULL,NULL,4075,NULL),(49,NULL,'css\\\\img\\\\49.jpg',NULL,NULL,NULL,NULL),(50,NULL,'wanted\\\\50.png',NULL,NULL,4090,NULL),(51,NULL,'wanted\\\\51.jpg',NULL,NULL,4091,NULL),(52,NULL,'wanted\\\\52.png',NULL,NULL,4099,NULL),(53,NULL,'wanted\\\\53.jpg',NULL,NULL,4100,NULL);
/*!40000 ALTER TABLE `file` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-08 16:59:53
